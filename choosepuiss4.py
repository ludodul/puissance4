﻿import random
import numpy as np

aligns = [[(0, 0), (1, 0), (2, 0), (3, 0)], [(0, 0), (0, 1), (0, 2), (0, 3)], [(0, 0), (1, 1), (2, 2), (3, 3)], [(0, 1), (1, 1), (2, 1), (3, 1)], [(0, 1), (0, 2), (0, 3), (0, 4)], [(0, 1), (1, 2), (2, 3), (3, 4)], [(0, 2), (1, 2), (2, 2), (3, 2)], [(0, 2), (0, 3), (0, 4), (0, 5)], [(0, 2), (1, 3), (2, 4), (3, 5)], [(0, 3), (1, 3), (2, 3), (3, 3)], [(0, 3), (0, 4), (0, 5), (0, 6)], [(0, 3), (1, 4), (2, 5), (3, 6)], [(0, 3), (1, 2), (2, 1), (3, 0)], [(0, 4), (1, 4), (2, 4), (3, 4)], [(0, 4), (1, 3), (2, 2), (3, 1)], [(0, 5), (1, 5), (2, 5), (3, 5)], [(0, 5), (1, 4), (2, 3), (3, 2)], [(0, 6), (1, 6), (2, 6), (3, 6)], [(0, 6), (1, 5), (2, 4), (3, 3)], [(1, 0), (2, 0), (3, 0), (4, 0)], [(1, 0), (1, 1), (1, 2), (1, 3)], [(1, 0), (2, 1), (3, 2), (4, 3)], [(1, 1), (2, 1), (3, 1), (4, 1)], [(1, 1), (1, 2), (1, 3), (1, 4)], [(1, 1), (2, 2), (3, 3), (4, 4)], [(1, 2), (2, 2), (3, 2), (4, 2)], [(1, 2), (1, 3), (1, 4), (1, 5)], [(1, 2), (2, 3), (3, 4), (4, 5)], [(1, 3), (2, 3), (3, 3), (4, 3)], [(1, 3), (1, 4), (1, 5), (1, 6)], [(1, 3), (2, 4), (3, 5), (4, 6)], [(1, 3), (2, 2), (3, 1), (4, 0)], [(1, 4), (2, 4), (3, 4), (4, 4)], [(1, 4), (2, 3), (3, 2), (4, 1)], [(1, 5), (2, 5), (3, 5), (4, 5)], [(1, 5), (2, 4), (3, 3), (4, 2)], [(1, 6), (2, 6), (3, 6), (4, 6)], [(1, 6), (2, 5), (3, 4), (4, 3)], [(2, 0), (3, 0), (4, 0), (5, 0)], [(2, 0), (2, 1), (2, 2), (2, 3)], [(2, 0), (3, 1), (4, 2), (5, 3)], [(2, 1), (3, 1), (4, 1), (5, 1)], [(2, 1), (2, 2), (2, 3), (2, 4)], [(2, 1), (3, 2), (4, 3), (5, 4)], [(2, 2), (3, 2), (4, 2), (5, 2)], [(2, 2), (2, 3), (2, 4), (2, 5)], [(2, 2), (3, 3), (4, 4), (5, 5)], [(2, 3), (3, 3), (4, 3), (5, 3)], [(2, 3), (2, 4), (2, 5), (2, 6)], [(2, 3), (3, 4), (4, 5), (5, 6)], [(2, 3), (3, 2), (4, 1), (5, 0)], [(2, 4), (3, 4), (4, 4), (5, 4)], [(2, 4), (3, 3), (4, 2), (5, 1)], [(2, 5), (3, 5), (4, 5), (5, 5)], [(2, 5), (3, 4), (4, 3), (5, 2)], [(2, 6), (3, 6), (4, 6), (5, 6)], [(2, 6), (3, 5), (4, 4), (5, 3)], [(3, 0), (3, 1), (3, 2), (3, 3)], [(3, 1), (3, 2), (3, 3), (3, 4)], [(3, 2), (3, 3), (3, 4), (3, 5)], [(3, 3), (3, 4), (3, 5), (3, 6)], [(4, 0), (4, 1), (4, 2), (4, 3)], [(4, 1), (4, 2), (4, 3), (4, 4)], [(4, 2), (4, 3), (4, 4), (4, 5)], [(4, 3), (4, 4), (4, 5), (4, 6)], [(5, 0), (5, 1), (5, 2), (5, 3)], [(5, 1), (5, 2), (5, 3), (5, 4)], [(5, 2), (5, 3), (5, 4), (5, 5)], [(5, 3), (5, 4), (5, 5), (5, 6)]]


def choose_align(grid):
    global aligns
    y = -1
    continuer = True
    list_score =[]
    for align in aligns:
        somme = 0
        continuer = True
        for coor in align:
            somme += grid[coor]
        list_score.append(somme)
    print(list_score)
    while -3 in list_score:
        index_align = list_score.index(-3)
        align = aligns[index_align]
        for coord in align:
            if grid[coord] == 0:

                if coord[0]==5:
                    y = coord[1]
                    list_score = tozeros(list_score,-3)
                elif grid[(coord[0]+1,coord[1])]==0:
                    y = -1
                    list_score[list_score.index(-3)]=0
                else:
                    y = coord[1]
                    list_score = tozeros(list_score,-3)

    if y == -1:
        while 3 in list_score:
            index_align = list_score.index(3)
            align = aligns[index_align]
            for coord in align:
                if grid[coord] == 0:

                    if coord[0]==5:
                        y = coord[1]
                        list_score = tozeros(list_score,3)
                    elif grid[(coord[0]+1,coord[1])]==0:
                        y = -1
                        list_score[list_score.index(3)]=0
                    else:
                        y = coord[1]
                        list_score = tozeros(list_score,3)

    if y == -1:
        liste_scores =[]
        for i in range(7):
            grid2 = copy_grid(grid)
            #print(grid2)
            y1 = remplir_grid(grid2,i)
            print(y1)
            if y1 != -1:
                grid2[y1][i] = -1
                print(grid2)
                liste_scores.append(somme_scores(grid2))
            else:
                liste_scores.append(1000)
        print(liste_scores)
        mini = min(liste_scores)
        y = liste_scores.index(mini)       
        print(y)
    return y

def somme_scores(grid):
    global aligns
    somme_pos = 0 
    for align in aligns:
        somme = 0
        for coord in align:
            somme += grid[coord]
        if somme > 0:
            somme_pos += 1
    return somme_pos


def tozeros(liste,nombre):
    new_liste = []
    for l in liste:
        if l == nombre:
            new_liste.append(0)
        else:
            new_liste.append(l)
    return new_liste


def remplir_grid(grid,y):
    column = grid[:,y]
    ligne = 5-np.count_nonzero(column)
    return ligne

def copy_grid(grid):
    grid2 = []
    for gri in grid:
        grid2.append(gri)
    return np.array(grid2)

