﻿from tkinter import *
import numpy as np
import random
import choosepuiss4
import choose2


fen = Tk()

aligns = [[(0, 0), (1, 0), (2, 0), (3, 0)], [(0, 0), (0, 1), (0, 2), (0, 3)], [(0, 0), (1, 1), (2, 2), (3, 3)], [(0, 1), (1, 1), (2, 1), (3, 1)], [(0, 1), (0, 2), (0, 3), (0, 4)], [(0, 1), (1, 2), (2, 3), (3, 4)], [(0, 2), (1, 2), (2, 2), (3, 2)], [(0, 2), (0, 3), (0, 4), (0, 5)], [(0, 2), (1, 3), (2, 4), (3, 5)], [(0, 3), (1, 3), (2, 3), (3, 3)], [(0, 3), (0, 4), (0, 5), (0, 6)], [(0, 3), (1, 4), (2, 5), (3, 6)], [(0, 3), (1, 2), (2, 1), (3, 0)], [(0, 4), (1, 4), (2, 4), (3, 4)], [(0, 4), (1, 3), (2, 2), (3, 1)], [(0, 5), (1, 5), (2, 5), (3, 5)], [(0, 5), (1, 4), (2, 3), (3, 2)], [(0, 6), (1, 6), (2, 6), (3, 6)], [(0, 6), (1, 5), (2, 4), (3, 3)], [(1, 0), (2, 0), (3, 0), (4, 0)], [(1, 0), (1, 1), (1, 2), (1, 3)], [(1, 0), (2, 1), (3, 2), (4, 3)], [(1, 1), (2, 1), (3, 1), (4, 1)], [(1, 1), (1, 2), (1, 3), (1, 4)], [(1, 1), (2, 2), (3, 3), (4, 4)], [(1, 2), (2, 2), (3, 2), (4, 2)], [(1, 2), (1, 3), (1, 4), (1, 5)], [(1, 2), (2, 3), (3, 4), (4, 5)], [(1, 3), (2, 3), (3, 3), (4, 3)], [(1, 3), (1, 4), (1, 5), (1, 6)], [(1, 3), (2, 4), (3, 5), (4, 6)], [(1, 3), (2, 2), (3, 1), (4, 0)], [(1, 4), (2, 4), (3, 4), (4, 4)], [(1, 4), (2, 3), (3, 2), (4, 1)], [(1, 5), (2, 5), (3, 5), (4, 5)], [(1, 5), (2, 4), (3, 3), (4, 2)], [(1, 6), (2, 6), (3, 6), (4, 6)], [(1, 6), (2, 5), (3, 4), (4, 3)], [(2, 0), (3, 0), (4, 0), (5, 0)], [(2, 0), (2, 1), (2, 2), (2, 3)], [(2, 0), (3, 1), (4, 2), (5, 3)], [(2, 1), (3, 1), (4, 1), (5, 1)], [(2, 1), (2, 2), (2, 3), (2, 4)], [(2, 1), (3, 2), (4, 3), (5, 4)], [(2, 2), (3, 2), (4, 2), (5, 2)], [(2, 2), (2, 3), (2, 4), (2, 5)], [(2, 2), (3, 3), (4, 4), (5, 5)], [(2, 3), (3, 3), (4, 3), (5, 3)], [(2, 3), (2, 4), (2, 5), (2, 6)], [(2, 3), (3, 4), (4, 5), (5, 6)], [(2, 3), (3, 2), (4, 1), (5, 0)], [(2, 4), (3, 4), (4, 4), (5, 4)], [(2, 4), (3, 3), (4, 2), (5, 1)], [(2, 5), (3, 5), (4, 5), (5, 5)], [(2, 5), (3, 4), (4, 3), (5, 2)], [(2, 6), (3, 6), (4, 6), (5, 6)], [(2, 6), (3, 5), (4, 4), (5, 3)], [(3, 0), (3, 1), (3, 2), (3, 3)], [(3, 1), (3, 2), (3, 3), (3, 4)], [(3, 2), (3, 3), (3, 4), (3, 5)], [(3, 3), (3, 4), (3, 5), (3, 6)], [(4, 0), (4, 1), (4, 2), (4, 3)], [(4, 1), (4, 2), (4, 3), (4, 4)], [(4, 2), (4, 3), (4, 4), (4, 5)], [(4, 3), (4, 4), (4, 5), (4, 6)], [(5, 0), (5, 1), (5, 2), (5, 3)], [(5, 1), (5, 2), (5, 3), (5, 4)], [(5, 2), (5, 3), (5, 4), (5, 5)], [(5, 3), (5, 4), (5, 5), (5, 6)]]
#print(len(aligns))

class Cell():
    def __init__(self, abs, ord):
        self.value = 0

    def case_rouge(self):
        self.value += 1

    def case_jaune(self):
        self.value -=1

class Joueur():
    def __init__(self):
        self.couleur = 'R'
        self.continuer = True

    def change_joueur(self):
        if self.couleur == 'R':
            self.couleur = 'J'
        else:
            self.couleur = 'R'



width=800
height=800

canvas=Canvas(fen,width=900, height=650)  # création d'un cadre de dessin dans la fenetre
canvas.pack(side=TOP)
for i in range(7):                         # création du damier
    canvas.create_line(100*(i)+5,5,100*(i)+5,605,width=2)
    canvas.create_line(5,100*(i)+5,705,100*(i)+5,width=2)
canvas.create_line(705,5,705,605,width=2)

canvas2 = Canvas(fen,width=900,height=20)
T = canvas2.create_text(350,10,text='appuyer sur nouvelle partie' )

canvas2.pack()



def callback(event):
    global T
    global joueur
    global cells
    global grid
    global cells_to_del

    x,y = get_coord(event)
    x_c,y_c = get_center(x,y)
    #print(x_c,y_c)
    x,y = get_cell_coord(x,y)
    #print(x,y)

    x1 = remplir_grid(x)
    #print(x1)
    if x1 == -1:
        print('colonne deja remplie')
    else:
        grid[x1][x] = 1
        x_c,y_c = get_center(x1,x)
        joueur.change_joueur()
        el = canvas.create_oval(y_c-27.5,x_c-27.5,y_c+32.5,x_c+32.5, fill = 'red',outline='red')
        cells_to_del.append(el)
        canvas2.delete(T)
        fen.update()
        if test_win():
            T = canvas2.create_text(350,10,text='joueur rouge gagne' )
            canvas.bind("<Button-1>", rien)
            joueur.continuer = False
        else:
            T = canvas2.create_text(350,10,text='joueur jaune' )

        if joueur.continuer:
            x = choose2.choos_cell(grid)
            #x = choosepuiss4.choose_align(grid)
            while remplir_grid(x) == -1:
                x = random.randint(0,6)
            x1 = remplir_grid(x)
            grid[x1][x] = -1
            x_c,y_c = get_center(x1,x)
            joueur.change_joueur()
            el = canvas.create_oval(y_c-27.5,x_c-27.5,y_c+32.5,x_c+32.5, fill = 'yellow',outline='yellow')
            cells_to_del.append(el)
            canvas2.delete(T)
            if test_win():
                T = canvas2.create_text(350,10,text='joueur jaune gagne' )
                canvas.bind("<Button-1>", rien)
                joueur.couleur = 'S'
            else:
                T = canvas2.create_text(350,10,text='joueur rouge' )
    print(grid)
    return 'toto'

def get_coord(event):
    x,y=event.x,event.y
    return x,y

def get_center(x,y):
    x_c = (x)*100 + 50
    y_c = (y)*100 +50
    return x_c,y_c


def get_cell_coord(x,y):
    x1 = (x-5)//100
    y1 = (y-5)//100
    return x1,y1

def remplir_grid(x):
    global grid
    column = grid[:,x]
    ligne = 5-np.count_nonzero(column)
    #print('ligne:',ligne)
    return ligne

def test_win():
    global grid
    global aligns
    for align in aligns:
        somme = 0
        for coor in align:
            somme += grid[coor]
        if somme == 4 or somme == -4:
            res = True
            break
        else:
            res = False
    return res

def rien(event):
    pass

def detruire():
    fen.destroy()

def begin_game():
    global T
    global joueur
    global grid
    global cells
    global cells_to_del
    try:
        for el in cells_to_del:
            canvas.delete(el)
    except:
        pass
    cells_to_del = []
    joueur = Joueur()
    cells = np.array([[Cell(i,j) for i in range(7)] for j in range(6)])
    grid = np.zeros((6,7))
    canvas.bind("<Button-1>", callback)
    canvas2.delete(T)
    T = canvas2.create_text(350,10,text='joueur Rouge' )
    return print('bonjour')



start = Button(fen, text="Nouvelle partie", command=begin_game)
start.pack(side=LEFT,padx=5, pady=5)
boutonquit=Button(fen,text='quitter',command=detruire)
boutonquit.pack(side=RIGHT,padx=5, pady=5)


fen.mainloop()
