import random
import numpy as np
#from numpy import random
from copy import copy

randomlist = [int(random.random()*7) for _ in range(5000)]
glob_index = 0

aligns = [[(0, 0), (1, 0), (2, 0), (3, 0)], [(0, 0), (0, 1), (0, 2), (0, 3)], [(0, 0), (1, 1), (2, 2), (3, 3)], [(0, 1), (1, 1), (2, 1), (3, 1)], [(0, 1), (0, 2), (0, 3), (0, 4)], [(0, 1), (1, 2), (2, 3), (3, 4)], [(0, 2), (1, 2), (2, 2), (3, 2)], [(0, 2), (0, 3), (0, 4), (0, 5)], [(0, 2), (1, 3), (2, 4), (3, 5)], [(0, 3), (1, 3), (2, 3), (3, 3)], [(0, 3), (0, 4), (0, 5), (0, 6)], [(0, 3), (1, 4), (2, 5), (3, 6)], [(0, 3), (1, 2), (2, 1), (3, 0)], [(0, 4), (1, 4), (2, 4), (3, 4)], [(0, 4), (1, 3), (2, 2), (3, 1)], [(0, 5), (1, 5), (2, 5), (3, 5)], [(0, 5), (1, 4), (2, 3), (3, 2)], [(0, 6), (1, 6), (2, 6), (3, 6)], [(0, 6), (1, 5), (2, 4), (3, 3)], [(1, 0), (2, 0), (3, 0), (4, 0)], [(1, 0), (1, 1), (1, 2), (1, 3)], [(1, 0), (2, 1), (3, 2), (4, 3)], [(1, 1), (2, 1), (3, 1), (4, 1)], [(1, 1), (1, 2), (1, 3), (1, 4)], [(1, 1), (2, 2), (3, 3), (4, 4)], [(1, 2), (2, 2), (3, 2), (4, 2)], [(1, 2), (1, 3), (1, 4), (1, 5)], [(1, 2), (2, 3), (3, 4), (4, 5)], [(1, 3), (2, 3), (3, 3), (4, 3)], [(1, 3), (1, 4), (1, 5), (1, 6)], [(1, 3), (2, 4), (3, 5), (4, 6)], [(1, 3), (2, 2), (3, 1), (4, 0)], [(1, 4), (2, 4), (3, 4), (4, 4)], [(1, 4), (2, 3), (3, 2), (4, 1)], [(1, 5), (2, 5), (3, 5), (4, 5)], [(1, 5), (2, 4), (3, 3), (4, 2)], [(1, 6), (2, 6), (3, 6), (4, 6)], [(1, 6), (2, 5), (3, 4), (4, 3)], [(2, 0), (3, 0), (4, 0), (5, 0)], [(2, 0), (2, 1), (2, 2), (2, 3)], [(2, 0), (3, 1), (4, 2), (5, 3)], [(2, 1), (3, 1), (4, 1), (5, 1)], [(2, 1), (2, 2), (2, 3), (2, 4)], [(2, 1), (3, 2), (4, 3), (5, 4)], [(2, 2), (3, 2), (4, 2), (5, 2)], [(2, 2), (2, 3), (2, 4), (2, 5)], [(2, 2), (3, 3), (4, 4), (5, 5)], [(2, 3), (3, 3), (4, 3), (5, 3)], [(2, 3), (2, 4), (2, 5), (2, 6)], [(2, 3), (3, 4), (4, 5), (5, 6)], [(2, 3), (3, 2), (4, 1), (5, 0)], [(2, 4), (3, 4), (4, 4), (5, 4)], [(2, 4), (3, 3), (4, 2), (5, 1)], [(2, 5), (3, 5), (4, 5), (5, 5)], [(2, 5), (3, 4), (4, 3), (5, 2)], [(2, 6), (3, 6), (4, 6), (5, 6)], [(2, 6), (3, 5), (4, 4), (5, 3)], [(3, 0), (3, 1), (3, 2), (3, 3)], [(3, 1), (3, 2), (3, 3), (3, 4)], [(3, 2), (3, 3), (3, 4), (3, 5)], [(3, 3), (3, 4), (3, 5), (3, 6)], [(4, 0), (4, 1), (4, 2), (4, 3)], [(4, 1), (4, 2), (4, 3), (4, 4)], [(4, 2), (4, 3), (4, 4), (4, 5)], [(4, 3), (4, 4), (4, 5), (4, 6)], [(5, 0), (5, 1), (5, 2), (5, 3)], [(5, 1), (5, 2), (5, 3), (5, 4)], [(5, 2), (5, 3), (5, 4), (5, 5)], [(5, 3), (5, 4), (5, 5), (5, 6)]]
SIMULATIONS = 1000


def somme_scores(grid):
    global aligns
    somme_pos = 0 
    for align in aligns:
        somme = 0
        for coord in align:
            somme += grid[coord]
        if somme > 0:
            somme_pos += 1
    return somme_pos


def tozeros(liste,nombre):
    # new_liste = []
    # for l in liste:
    #     if l == nombre:
    #         new_liste.append(0)
    #     else:
    #         new_liste.append(l)
    return [0 if l == nombre else l for l in liste ]


def remplir_grid(grid,y):
    column = grid[:,y]
    ligne = 5-np.count_nonzero(column)
    return ligne

def copy_grid(grid):
    grid2 = []
    for gri in grid:
        grid2.append(gri)
    return np.array(grid2)


def choose_rand2(grid):
    global aligns
    global glob_index
    y = -1
    # list_score =[]
    # for align in aligns:
    #     somme = 0
    #     continuer = True
    #     for coor in align:
    #         somme += grid[coor]
    #     list_score.append(somme)
    list_score = [sum(grid[coor] for coor in align) for align in aligns] 
    #print(list_score)
    while -3 in list_score:
        index_align = list_score.index(-3)
        align = aligns[index_align]
        for coord in align:
            if grid[coord] == 0:

                if coord[0]==5:
                    y = coord[1]
                    list_score = tozeros(list_score,-3)
                elif grid[(coord[0]+1,coord[1])]==0:
                    y = -1
                    list_score[list_score.index(-3)]=0
                else:
                    y = coord[1]
                    list_score = tozeros(list_score,-3)

    if y == -1:
        while 3 in list_score:
            index_align = list_score.index(3)
            align = aligns[index_align]
            for coord in align:
                if grid[coord] == 0:

                    if coord[0]==5:
                        y = coord[1]
                        list_score = tozeros(list_score,3)
                    elif grid[(coord[0]+1,coord[1])]==0:
                        y = -1
                        list_score[list_score.index(3)]=0
                    else:
                        y = coord[1]
                        list_score = tozeros(list_score,3)

    if y == -1:
        #y = random.randint(0,6)
        #y = int(random.random()*7)
        y = randomlist[glob_index%5000]
        glob_index +=1
    return y

def choose_rand(grid):
    global aligns
    global glob_index
    y = -1
    # list_score = []
    # for align in aligns:
    #     somme=sum(grid[coor] for coor in align)
    #     list_score.append(somme)
    list_score = [sum(grid[coor] for coor in align) for align in aligns] 
    #print(list_score)
    while 3 in list_score:
        index_align = list_score.index(3)
        align = aligns[index_align]
        for coord in align:
            if grid[coord] == 0:

                if coord[0]==5:
                    y = coord[1]
                    list_score = tozeros(list_score,3)
                elif grid[(coord[0]+1,coord[1])]==0:
                    y = -1
                    list_score[list_score.index(3)]=0
                else:
                    y = coord[1]
                    list_score = tozeros(list_score,3)

    if y == -1:
        while -3 in list_score:
            index_align = list_score.index(-3)
            align = aligns[index_align]
            for coord in align:
                if grid[coord] == 0:

                    if coord[0]==5:
                        y = coord[1]
                        list_score = tozeros(list_score,-3)
                    elif grid[(coord[0]+1,coord[1])]==0:
                        y = -1
                        list_score[list_score.index(-3)]=0
                    else:
                        y = coord[1]
                        list_score = tozeros(list_score,-3)

    if y == -1:
        #y = random.randint(0,6)
        #y = int(random.random()*7)
        y = randomlist[glob_index%5000]
        glob_index +=1
    return y


def partie(grid1, nombre):
    score_R = 0
    score_J = 0
    score_eg = 0
    dictio = {}
    for i in range(nombre):
        grid = copy(grid1)
        #grid = copy_grid(grid1)
        continuer = True
        while continuer:
            x = choose_rand(grid)
            x1 = remplir_grid(grid,x)
            while x1 == -1:
                x = random.randint(0,6)
                x1 = remplir_grid(grid,x)
            grid[x1][x] = 1
            #print(x,x1)
            #print(grid)
            if test_win(grid)['res']:
                somme = test_win(grid)['W']
                if somme == 4:
                    score_R += 1 
                    continuer = False
                else:
                    score_J += 1
                    continuer = False
            elif np.count_nonzero(grid) == 42:
                continuer = False
                score_eg += 1
            else:
                x = choose_rand2(grid)
                x1 = remplir_grid(grid,x)
                while x1 == -1:
                    x = random.randint(0,6)
                    x1 = remplir_grid(grid,x)
                grid[x1][x] = -1
                #print(grid)
                if test_win(grid)['res']:
                    somme = test_win(grid)['W']
                    if somme == 4:
                        score_R += 1 
                        continuer = False
                    else:
                        score_J += 1
                        continuer = False
                elif np.count_nonzero(grid) == 42:
                    continuer = False
                    score_eg += 1
    print('score_R:',score_R)     
    print('score_J:',score_J)
    print('score_eg:',score_eg)
    return {'score_R':score_R,
            'score_J':score_J,
            'score_eg':score_eg
               }
            

def choos_cell(grid):
    liste_prob =[]
    liste_prob_J = []
    nbr = SIMULATIONS
    for i in range(7):
        grid1 = copy_grid(grid)
        y = remplir_grid(grid1,i)
        if y == -1:
            liste_prob.append(2)
            liste_prob_J.append(0)
        else:
            grid1[y][i]=-1
            RES = test_win(grid1)
            if RES['res']:
                return i
            else:
                probs = partie(grid1,nbr)
                liste_prob.append(probs['score_R']/nbr)
                liste_prob_J.append(probs['score_J']/nbr)
    if 1 in liste_prob_J:
        res = liste_prob_J.index(1)
    else:
        res = liste_prob.index(min(liste_prob))
    return res

def somme_scores(grid):
    global aligns
    somme_pos = 0 
    for align in aligns:
        somme = 0
        for coord in align:
            somme += grid[coord]
        if somme > 0:
            somme_pos += 1
    return somme_pos


def tozeros(liste,nombre):
    new_liste = []
    for l in liste:
        if l == nombre:
            new_liste.append(0)
        else:
            new_liste.append(l)
    return new_liste


def remplir_grid(grid,y):
    column = grid[:,y]
    ligne = 5-np.count_nonzero(column)
    return ligne

def copy_grid(grid):
    grid2 = []
    for gri in grid:
        grid2.append(gri)
    return np.array(grid2)

def test_win(grid):    
    global aligns
    res = False
    for align in aligns:
        somme = sum((grid[coor] for coor in align))
        res = somme in [4,-4]
        if res:
            break
    return {'res':res,'W':somme}